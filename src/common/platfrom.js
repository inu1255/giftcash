import store from '../store/index'
import Bmob from "hydrogen-js-sdk";

/**
 * @returns {User}
 */
export function goToLogin() {
	// #ifdef MP-WEIXIN
	return Bmob.User.auth().then(res => {
		store.commit('login', res.nickName)
		return res
	});
	// #endif
	// #ifndef MP-WEIXIN
	store.commit('navigateTo', '/pages/login/login')
	// #endif
}