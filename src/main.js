import Vue from 'vue'
import App from './App'

import store from './store'
import './common/bmob'

Vue.config.productionTip = false

Vue.prototype.$store = store
Vue.prototype.$toast = {
	show(title, icon, duration) {
		if (typeof duration != "number")
			duration = 1e3 + title.length * 200;
		var image = ['success', 'loading', 'none'].indexOf(icon) < 0 ? icon : '';
		return new Promise(function(resolve, reject) {
			uni.showToast({ title, duration, icon, image, success: resolve, fail: reject })
		});
	},
	success(title, duration) {
		this.show(title, 'success', duration)
	},
	error(title, duration) {
		this.show(title, '', duration)
	}
}

App.mpType = 'app'

const app = new Vue({
	store,
	...App
})
app.$mount()